package com.jvm.jvmw1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JvmW1Application {

	public static void main(String[] args) {
		SpringApplication.run(JvmW1Application.class, args);
	}

}
