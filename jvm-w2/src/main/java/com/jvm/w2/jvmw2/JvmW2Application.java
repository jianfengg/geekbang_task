package com.jvm.w2.jvmw2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JvmW2Application {

	public static void main(String[] args) {
		SpringApplication.run(JvmW2Application.class, args);
	}

}
