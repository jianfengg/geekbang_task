CREATE DATABASE tb_order DEFAULT CHARSET utf8mb4;

USE tb_order;

CREATE TABLE tb_user (
	`id` BIGINT NOT NULL AUTO_INCREMENT COMMENT '主键',
    `name` VARCHAR(50) NOT NULL COMMENT '用户昵称',
    `phone` VARCHAR(50) NOT NULL COMMENT '手机号码',
    `del_flag` INT ( 1 ) DEFAULT '0' COMMENT '删除标志（0:未删除,1代表删除）',
	`create_by` VARCHAR ( 64 ) DEFAULT '' COMMENT '创建者',
	`create_time` datetime DEFAULT NULL COMMENT '创建时间',
	`update_by` VARCHAR ( 64 ) DEFAULT '' COMMENT '更新者',
	`update_time` datetime DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (id),
    UNIQUE KEY (phone)
) ENGINE=InnoDB, DEFAULT CHARSET=utf8mb4, COMMENT '用户数据表';


CREATE TABLE `tb_good_cat` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `cat_name` varchar(64) NOT NULL COMMENT '分类名称',
  `sort` int(5) DEFAULT '50' COMMENT '排序',
  `del_flag` int(1) DEFAULT '0' COMMENT '删除标志（0:未删除,1代表删除）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='商品分类';

CREATE TABLE tb_goods (
	id BIGINT NOT NULL AUTO_INCREMENT COMMENT '主键',
    cat_id BIGINT NOT NULL COMMENT '商品分类',
    name VARCHAR(50) NOT NULL COMMENT '商品名称',
    `del_flag` int(1) DEFAULT '0' COMMENT '删除标志（0:未删除,1代表删除）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (id)
) ENGINE=InnoDB, DEFAULT CHARSET=utf8mb4, COMMENT '商品表';


CREATE TABLE tb_stock (
	id BIGINT NOT NULL AUTO_INCREMENT COMMENT '主键',
    good_id BIGINT NOT NULL COMMENT '关联商品ID',
    storage_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP() COMMENT '入库时间',
    retrieval_time TIMESTAMP NULL COMMENT '出库时间',
    `status` INT NOT NULL COMMENT '库存状态，0入库中，1已入库，2锁定，8报损，9已出库',
    PRIMARY KEY (id),
    FOREIGN KEY (good_id) REFERENCES t_goods(id)
) ENGINE=InnoDB, DEFAULT CHARSET=utf8mb4, COMMENT '库存表';


CREATE TABLE `tb_order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` bigint(20) NOT NULL COMMENT '订单编号',
  `order_no` varchar(64) NOT NULL COMMENT '订单编号',
  `type` int(2) NOT NULL DEFAULT '0' COMMENT '订单类型',
  `cat_id` bigint(20) NOT NULL COMMENT '商品分类',
  `amount` int(11) NOT NULL DEFAULT '0' COMMENT '金额',
  `order_status` int(3) NOT NULL DEFAULT '0' COMMENT '状态(0取消,1:已生成,2:已关闭,3:已退款)',
  `check_time` datetime DEFAULT NULL COMMENT '完成时间',
  `memo` varchar(255) DEFAULT NULL COMMENT '备注',
  `del_flag` int(1) DEFAULT '0' COMMENT '删除标志（0:未删除,1代表删除）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='订单表';