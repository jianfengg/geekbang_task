package com.nio.w3.niow3.filter;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpUtil;
import lombok.extern.slf4j.Slf4j;

import static io.netty.handler.codec.http.HttpHeaderNames.CONTENT_LENGTH;
import static io.netty.handler.codec.http.HttpHeaderNames.CONTENT_TYPE;
import static io.netty.handler.codec.http.HttpResponseStatus.NO_CONTENT;
import static io.netty.handler.codec.http.HttpVersion.HTTP_1_1;

@Slf4j
public class HeaderHttpRequestFilter extends ChannelInboundHandlerAdapter implements HttpRequestFilter {

    @Override
    public void filter(FullHttpRequest fullRequest, ChannelHandlerContext ctx) {

        // 设置自定义请求头
        fullRequest.headers().set("my-test", "my-test-req");

        String uri = fullRequest.uri();
        if (uri.equals("/test")) {
            // 拦截 test
            FullHttpResponse response = new DefaultFullHttpResponse(
                    HTTP_1_1, NO_CONTENT, Unpooled.EMPTY_BUFFER);
            response.headers().set(CONTENT_TYPE, "text/plain; charset=utf-8");
            response.headers().setInt(CONTENT_LENGTH, response.content().readableBytes());
            if (!HttpUtil.isKeepAlive(fullRequest)) {
                ctx.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE);
            } else {
                ctx.writeAndFlush(response);
            }
        } else {
            // 其他请求继续
            try {
                super.channelRead(ctx, fullRequest);
            } catch (Exception e) {
                log.error("req filter err", e);
            }
        }

    }
}
