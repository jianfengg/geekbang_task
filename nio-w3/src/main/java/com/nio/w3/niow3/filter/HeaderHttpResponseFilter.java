package com.nio.w3.niow3.filter;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOutboundHandlerAdapter;
import io.netty.channel.ChannelPromise;
import io.netty.handler.codec.http.FullHttpResponse;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class HeaderHttpResponseFilter extends ChannelOutboundHandlerAdapter implements HttpResponseFilter {
    @Override
    public void filter(FullHttpResponse response) {
        response.headers().set("my-test", "my-test-res");
    }

    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
        log.info("my outbound filter write");
        if(msg instanceof FullHttpResponse){
            FullHttpResponse response = (FullHttpResponse)msg;
            // 添加自定义响应头
            filter(response);
        } else {
            log.info("outbound msg: {}",msg);
        }
        super.write(ctx, msg, promise);
    }
}
