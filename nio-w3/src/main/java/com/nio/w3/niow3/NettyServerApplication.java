package com.nio.w3.niow3;

import com.nio.w3.niow3.common.CommonConstants;
import com.nio.w3.niow3.inbound.HttpInboundServer;

import java.util.Arrays;

/**
 * 自定义网关
 */
public class NettyServerApplication {

    public static void main(String[] args) {
        String proxyPort = System.getProperty("proxyPort","8888");

        String proxyServers = System.getProperty("proxyServers","http://localhost:8801,http://localhost:8802");
        int port = Integer.parseInt(proxyPort);
        System.out.println(CommonConstants.GATEWAY_NAME + " " + CommonConstants.GATEWAY_VERSION +" starting...");
        HttpInboundServer server = new HttpInboundServer(port, Arrays.asList(proxyServers.split(",")));
        System.out.println(CommonConstants.GATEWAY_NAME + " " + CommonConstants.GATEWAY_VERSION +" started at http://localhost:" + port + " for server:" + server.toString());
        try {
            server.run();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

}
