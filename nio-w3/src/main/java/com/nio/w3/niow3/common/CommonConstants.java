package com.nio.w3.niow3.common;

public class CommonConstants {

    private CommonConstants() {
        super();
    }

    public static final String GATEWAY_NAME = "NIO_GATEWAY";

    public final static String GATEWAY_VERSION = "1.0.0";

}
