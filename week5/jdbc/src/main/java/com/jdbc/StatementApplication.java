package com.jdbc;

import java.sql.*;

public class StatementApplication {
	
	/**
	 * 数据库URL
	 */
	private static final String DB_URL = "jdbc:mysql://localhost:3306/test?useSSL=false&serverTimezone=Asia/Shanghai&useUnicode=true&characterEncoding=UTF-8";

	/**
	 * 数据库用户名
	 */
	private static final String DB_USER = "root";
	
	/**
	 * 数据库密码
	 */
	private static final String DB_PASSWORD = "111111";

	private Connection connection = null;
	private Statement statement = null;

	public void createConnection() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			try {
				connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
				statement = connection.createStatement();
			} catch (SQLException throwables) {
				throwables.printStackTrace();
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.out.println("ClassNotFoundException");
		}
	}

	private void closeConnection() throws SQLException {
		if (null != connection) {
		    connection.close();
		}
		if (null != statement) {
			statement.close();
		}
	}

	private void query() throws SQLException {
		String strSQL = "SELECT id, user, user_create_time FROM t_user WHERE id = 1";
		ResultSet rs = statement.executeQuery(strSQL);
		while(rs.next()) {
			System.out.println(rs.getLong("id") + "," + rs.getString("user") + "," + rs.getTimestamp("user_create_time"));
		}
		rs.close();
	}


	public void insert() throws SQLException {
		String strSQL = "INSERT INTO t_user (user) VALUES ('user11')";
		int rows = statement.executeUpdate(strSQL);
		System.out.println(rows);
	}

	public void update() throws SQLException {
		String strSQL = "UPDATE t_user SET user = 'user21' WHERE id = 8";
		int rows = statement.executeUpdate(strSQL);
		System.out.println(rows);
	}

	public void delete() throws SQLException {
		String strSQL = "DELETE FROM t_user WHERE id = 8";
		int rows = statement.executeUpdate(strSQL);
		System.out.println(rows);
	}


	public static void main(String[] args) throws Exception {
		StatementApplication application = new StatementApplication();
		application.createConnection();
		application.insert();
		application.update();
		application.delete();
		application.closeConnection();
	}
}
