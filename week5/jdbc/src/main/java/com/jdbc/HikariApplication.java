package com.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class HikariApplication {
	
	/**
	 * 数据库URL
	 */
	private static final String DB_URL = "jdbc:mysql://localhost:3306/test?useSSL=false&serverTimezone=Asia/Shanghai&useUnicode=true&characterEncoding=UTF-8";

	/**
	 * 数据库用户名
	 */
	private static final String DB_USER = "root";
	
	/**
	 * 数据库密码
	 */
	private static final String DB_PASSWORD = "654321";
	
	public static void main(String[] args) throws Exception {
		HikariConfig config = buildConfig();
		HikariDataSource dataSource = new HikariDataSource(config);
		
		String strSQL = "SELECT user FROM t_user WHERE id = ?";
		Connection connection = dataSource.getConnection();
		PreparedStatement statement = connection.prepareStatement(strSQL);
		statement.setLong(1, 1L);
		
		ResultSet rs = statement.executeQuery();
		while(rs.next()) {
			System.out.println("user name:" + rs.getString("user"));
		}
		dataSource.close();
	}

	/**
	 * 构造连接池配置
	 * @return 连接池配置
	 */
	private static HikariConfig buildConfig() {
		HikariConfig config = new HikariConfig();
		config.setJdbcUrl(DB_URL);
		config.setUsername(DB_USER);
		config.setPassword(DB_PASSWORD);
		config.setPoolName("testPool");
		config.setMinimumIdle(1);
		config.setMaximumPoolSize(10);
		config.setIdleTimeout(10 * 6000);
		return config;
	}
}
