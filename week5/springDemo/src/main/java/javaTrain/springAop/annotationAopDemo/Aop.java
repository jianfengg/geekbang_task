package javaTrain.springAop.annotationAopDemo;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;

@Aspect
public class Aop {
    @Pointcut(value="execution(* javaTrain.springAop.xmlAopDemo.aop.*.*(..))")
    public void point(){
        System.out.println("=========point=========");
    }
    /**
     * 前置通知
     */
    @Before(value="point()")
    public void beforeNotice(){
        System.out.println("=========beforeNotice2=========");
    }

    /**
     * 后置通知
     */
    @AfterReturning(value = "point()")
    public void afterNotice(){
        System.out.println("=========afterNotice2=========");
    }

    /**
     * 环绕通知
     */
    @Around(value = "point()")
    public void aroundNotice(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println("=========aroundNotice1=========");
        joinPoint.proceed();
        System.out.println("=========aroundNotice2=========");
    }
}
