package javaTrain.springAop.xmlAopDemo;

import javaTrain.springAop.xmlAopDemo.aop.ISchool;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestXmlAop {

    public static void main(String[] args) {
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("applicationContext_xmlAop.xml");
        ISchool school= (ISchool) applicationContext.getBean("school");
        school.ding();
    }
}
