package javaTrain.springAop.xmlAopDemo;

import org.aspectj.lang.ProceedingJoinPoint;

public class Aop {

    /**
     * 前置通知
     */
    public void beforeNotice(){
        System.out.println("=========beforeNotice=========");
    }

    /**
     * 后置通知
     */
    public void afterNotice(){
        System.out.println("=========afterNotice=========");
    }

    /**
     * 环绕通知
     */
    public void aroundNotice(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println("=========aroundNotice1=========");
        joinPoint.proceed();
        System.out.println("=========aroundNotice2=========");
    }

}
