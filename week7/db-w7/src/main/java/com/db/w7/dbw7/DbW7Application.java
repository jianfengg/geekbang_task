package com.db.w7.dbw7;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DbW7Application {

	public static void main(String[] args) {
		SpringApplication.run(DbW7Application.class, args);
	}

}
