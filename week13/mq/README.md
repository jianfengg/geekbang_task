# 消息队列 Demo
***
## 简介
实现消息队列 Demo 程序

## 设计思路
大致的架构图如下：

![](./picture/framework.png)

如上图所示，消息队列Dome的总体架构分为三个：

- Broker：消息队列中心，存放着所有数据；接收生产者的数据；发送数据给消费者；等等核心功能
- 通信协议层：这次是定义生产者、消费者与Broker之间的通信方式，比如HTTP、TCP、Websocket等等，后面会对应进行性能测试
- 生产者和消费者：提供相应的API给用户进行调用

## 工程结构
- core ：消息队列功能API实现

```shell script
├─src
│  ├─main
│  │  ├─java
│  │  │  └─com
│  │  │      └─mq
│  │  │          └─core
│  │  │              └─core
│  │  │                  ├─consumer : 消费者API
│  │  │                  ├─producer : 生产者API
│  │  │                  ├─messagequeue : Broker，消息队列具体实现
│  │  │                  ├─protocol : Broker，消息队列具体实现
│  │  │                     ├─controller : HTTP协议通信
│  │  │                     └─websocket : Websocket协议通信
```

- example ： 使用示例，并进行相应的测试；使用消息队列API，实现生产和消费

## 程序运行说明
直接运行Example工程下的ExampleApplication即可

## 第二个版本：自定义 Queue
- [x] 2、去掉内存Queue，设计自定义Queue，实现消息确认和消费offset
    - [x] 1）自定义内存Message数组模拟Queue。
    - [x] 2）使用指针记录当前消息写入位置。
    - [x] 3）对于每个命名消费者，用指针记录消费位置。
    
#### 测试记录
生产者和消费者通信方式：

- producer use websocket
- consumer use http

##### 2.1 自定义Queue：悲观读写锁


```text
start producer test
Producer 100000 messages spend time : 477 ms 
Start consumer test
Consumer 100000 messages spend time : 420 ms
```

##### 2.1 自定义Queue：悲观写、自旋锁读
```text
start producer test
Producer 100000 messages spend time : 429 ms 
Start consumer test
Consumer 100000 messages spend time : 332 ms
```