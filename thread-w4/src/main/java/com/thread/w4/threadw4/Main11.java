package com.thread.w4.threadw4;

import com.thread.w4.threadw4.constant.CommonConstants;

import java.util.concurrent.*;

public class Main11 {

    private static final Semaphore semaphore = new Semaphore(0);


    public static void main(String[] args) throws Exception {
        //利用Callable实现，Future拿到返回值
        long start = System.currentTimeMillis();

        ExecutorService executorService = Executors.newSingleThreadExecutor();

        Callable<Integer> task = () -> {
            try {
                TimeUnit.SECONDS.sleep(CommonConstants.SLEEP_ONE);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return 1;
        };
        Future<Integer> future = executorService.submit(task);
        System.out.println("异步计算结果：" + future.get());
        System.out.println("计算耗时：" + (System.currentTimeMillis() - start) + "  ms");
        executorService.shutdown();
    }
}
