package com.thread.w4.threadw4;

import com.thread.w4.threadw4.constant.CommonConstants;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

public class Main9 {

    private static final CyclicBarrier barrier = new CyclicBarrier(2);


    public static void main(String[] args) throws Exception {
        //CyclicBarrier。主线程启动计算线程之后，执行await,之后计算线程执行完之后，也执行await,这个内存屏障设为2，则正好解除屏障，继续执行
        long start = System.currentTimeMillis();

        SumThread sumThread = new SumThread();
        sumThread.start();

        barrier.await();

        int result = sumThread.getResult();
        System.out.println("异步计算结果：" + result);
        System.out.println("计算耗时：" + (System.currentTimeMillis() - start) + "  ms");
    }

    static class SumThread extends Thread {

        private int result;

        public Integer getResult() {
            return result;
        }

        @Override
        public void run() {
            try {
                TimeUnit.SECONDS.sleep(CommonConstants.SLEEP_ONE);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            try {
                result += 1;
                barrier.await();
            } catch (InterruptedException | BrokenBarrierException e) {
                e.printStackTrace();
            }
        }
    }
}
