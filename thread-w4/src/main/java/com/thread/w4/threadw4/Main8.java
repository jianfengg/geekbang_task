package com.thread.w4.threadw4;

import com.thread.w4.threadw4.constant.CommonConstants;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class Main8 {

    private static final CountDownLatch latch = new CountDownLatch(1);

    public static void main(String[] args) throws Exception {
        //定义一个CountDownLatch，需要倒计时的线程为1，当main线程启动线程之后，让CountDownLatch执行await方法，计算线程在计算完毕之后，执行countdown方法。mian线程则会继续执行
        long start = System.currentTimeMillis();

        SumThread sumThread = new SumThread();
        sumThread.start();

        latch.await();

        int result = sumThread.getResult();
        System.out.println("异步计算结果：" + result);
        System.out.println("计算耗时：" + (System.currentTimeMillis() - start) + "  ms");
    }

    static class SumThread extends Thread {

        private int result;

        public Integer getResult() {
            return result;
        }

        @Override
        public void run() {
            try {
                TimeUnit.SECONDS.sleep(CommonConstants.SLEEP_ONE);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            result += 1;
            latch.countDown();
        }
    }
}
