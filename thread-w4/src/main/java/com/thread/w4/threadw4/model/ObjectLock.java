package com.thread.w4.threadw4.model;

public class ObjectLock {

    private Object lock;

    public Object getLock() {
        return lock;
    }

    public ObjectLock() {
        lock = new Object();
    }
}
