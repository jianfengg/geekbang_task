package com.thread.w4.threadw4;

import com.thread.w4.threadw4.constant.CommonConstants;

import java.util.concurrent.TimeUnit;

public class Main4 extends Thread {

    //创建线程时new一个全新对象作为加锁的条件
    private static final Object lock = new Object();

    //不赋值，默认值为0
    private int result;


    public int getResult() {
        return result;
    }

    @Override
    public void run() {
        //计算时加锁
        synchronized (lock) {
            try {
                TimeUnit.SECONDS.sleep(CommonConstants.SLEEP_ONE);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            result += 1;
            lock.notifyAll();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        //利用object对象的notify方法来实现。在启动完计算线程之后将主线程wait,之后在计算线程中，执行完毕之后，调用notify/notifyAll方法来唤醒主线程继续执行
        long start = System.currentTimeMillis();

        Main4 main = new Main4();

        main.start();


        synchronized (lock) {
            lock.wait();
        }

        //等待计算线程执行完毕
        int result = main.getResult();


        System.out.printf("等待结果: %s \n", result);

        System.out.println("计算耗时：" + (System.currentTimeMillis() - start) + "  ms");
    }
}
