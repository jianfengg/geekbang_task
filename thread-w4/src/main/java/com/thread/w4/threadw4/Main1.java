package com.thread.w4.threadw4;


import com.thread.w4.threadw4.constant.CommonConstants;

import java.util.concurrent.TimeUnit;

public class Main1 extends Thread {

    //不赋值，默认值为0
    private int result;

    public int getResult() {
        return result;
    }

    @Override
    public void run() {
        try {
            TimeUnit.SECONDS.sleep(CommonConstants.SLEEP_ONE);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        result += 1;
    }

    public static void main(String[] args) throws InterruptedException {
        //通过join方法，让当前线程进行阻塞，等待被执行线程结束之后再执行的方法
        long start = System.currentTimeMillis();

        Main1 main = new Main1();

        main.start();

        int result = main.getResult();

        System.out.printf("不等待结果: %s  \n", result);


        main.join();

        result = main.getResult();

        System.out.printf("等待结果: %s \n", result);

        System.out.println("计算耗时：" + (System.currentTimeMillis() - start) + "  ms");

    }

}
