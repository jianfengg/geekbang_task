package com.thread.w4.threadw4;

import com.thread.w4.threadw4.constant.CommonConstants;

import java.util.concurrent.TimeUnit;

public class Main2 extends Thread {


    //不赋值，默认值为0
    private int result;

    private volatile boolean success = false;

    public boolean isSuccess() {
        return success;
    }

    public int getResult() {
        return result;
    }

    @Override
    public void run() {
        try {
            TimeUnit.SECONDS.sleep(CommonConstants.SLEEP_ONE);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        result += 1;
        success = true;
    }

    public static void main(String[] args) throws InterruptedException {
        //使用volatile控制共享变量，计算线程在计算完成之后，更新这个volatile变量的状态为ture，那么main线程只需要在计算线程启动之后，不断轮询监控该变量的状态即可
        long start = System.currentTimeMillis();

        Main2 main = new Main2();

        main.start();

        int result = main.getResult();

        System.out.printf("不等待结果: %s  \n", result);

        while (!main.isSuccess()) {
            TimeUnit.MILLISECONDS.sleep(CommonConstants.SLEEP_ONE  );
        }

        result = main.getResult();

        System.out.printf("等待结果: %s \n", result);

        System.out.println("计算耗时：" + (System.currentTimeMillis() - start) + "  ms");

    }
}
