package com.thread.w4.threadw4;

import com.thread.w4.threadw4.constant.CommonConstants;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class Main10 {

    private static final Semaphore semaphore = new Semaphore(0);


    public static void main(String[] args) throws Exception {
        //Semaphore，Semaphore初始为0，在主线程中执行acquire，自然会被阻塞，等到计算线程执行完毕，执行release
        long start = System.currentTimeMillis();

        SumThread sumThread = new SumThread();
        sumThread.start();

        semaphore.acquire();

        int result = sumThread.getResult();
        System.out.println("异步计算结果：" + result);
        System.out.println("计算耗时：" + (System.currentTimeMillis() - start) + "  ms");
    }

    static class SumThread extends Thread {

        private int result;

        public Integer getResult() {
            return result;
        }

        @Override
        public void run() {
            try {
                TimeUnit.SECONDS.sleep(CommonConstants.SLEEP_ONE);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            try {
                result += 1;
                semaphore.release();
            } catch (Exception  e) {
                e.printStackTrace();
            }
        }
    }
}
