package com.thread.w4.threadw4;

import com.thread.w4.threadw4.constant.CommonConstants;
import com.thread.w4.threadw4.model.ObjectLock;

import java.util.concurrent.TimeUnit;

public class Main3 extends Thread {

    //创建线程时new一个全新对象作为加锁的条件
    private static final ObjectLock lock = new ObjectLock();

    //不赋值，默认值为0
    private int result;


    public int getResult() {
        return result;
    }

    @Override
    public void run() {
        //计算时加锁
        synchronized (lock) {
            try {
                TimeUnit.SECONDS.sleep(CommonConstants.SLEEP_ONE);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            result += 1;
        }
    }

    public static void main(String[] args) throws InterruptedException {
        //使用 synchronized 关键字，让计算线程先拿到锁，这之后main线程被synchronized阻塞。直到计算线程执行完毕
        long start = System.currentTimeMillis();

        Main3 main = new Main3();

        main.start();

        int result = main.getResult();

        System.out.printf("不等待结果: %s  \n", result);

        //等待计算线程执行完毕
        synchronized (lock) {

        }

        result = main.getResult();

        System.out.printf("等待结果: %s \n", result);

        System.out.println("计算耗时：" + (System.currentTimeMillis() - start) + "  ms");

    }
}
