package com.thread.w4.threadw4;

import com.thread.w4.threadw4.constant.CommonConstants;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class Main7 {

    private static final ReentrantLock lock = new ReentrantLock(true);
    private static final Condition c1 = lock.newCondition();

    public static void main(String[] args) throws Exception {
        long start = System.currentTimeMillis();
        SumThread sumThread = new SumThread();
        sumThread.start();

        lock.lock();
        try {
            c1.await();
        } finally {
            lock.unlock();
        }

        int result = sumThread.getResult();
        System.out.println("异步计算结果：" + result);
        System.out.println("计算耗时：" + (System.currentTimeMillis() - start) + "  ms");
    }

    static class SumThread extends Thread {

        private int result;

        public Integer getResult() {
            return result;
        }

        @Override
        public void run() {
            lock.lock();
            try {
                try {
                    TimeUnit.SECONDS.sleep(CommonConstants.SLEEP_ONE);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                result += 1;
                c1.signalAll();
            } finally {
                lock.unlock();
            }
        }
    }
}
