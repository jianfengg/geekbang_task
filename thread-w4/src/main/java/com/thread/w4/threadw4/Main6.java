package com.thread.w4.threadw4;

import com.thread.w4.threadw4.constant.CommonConstants;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

public class Main6 extends Thread {

    private Thread mainThread;

    public Thread getMainThread() {
        return mainThread;
    }

    public void setMainThread(Thread mainThread) {
        this.mainThread = mainThread;
    }

    //不赋值，默认值为0
    private int result;


    public int getResult() {
        return result;
    }

    @Override
    public void run() {
        try {
            TimeUnit.SECONDS.sleep(CommonConstants.SLEEP_ONE);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        result += 1;
        LockSupport.unpark(getMainThread());
    }

    public static void main(String[] args) {
        //利用同步工具中的lockSupport的park/unpark方法来实现。在主线程启动计算线程之后执行park,之后再在计算线程执行完毕之后，调用主线程的unpark方法
        long start = System.currentTimeMillis();

        Main6 main = new Main6();
        main.setMainThread(Thread.currentThread());
        main.start();
        LockSupport.park();
        int result = main.getResult();

        System.out.printf("不等待结果: %s  \n", result);

        //等待计算线程执行完毕
        result = main.getResult();

        System.out.printf("等待结果: %s \n", result);

        System.out.println("计算耗时：" + (System.currentTimeMillis() - start) + "  ms");
    }
}
